��    (      \  5   �      p     q      y  )   �     �     �  F   �  ?   %     e     m     t     |     �     �  �   �     G     U     a     h  .   �  +   �  
   �     �     �     �          +     0     N  
   i     t     {     �     �  $   �  :   �       !   3  *   U  .   �    �     �	  -   �	  :   �	     :
     N
  Y   h
  A   �
          	               (     ?  �   V               )  "   ;  D   ^  A   �     �     �     �          %     @  *   G  '   r     �  	   �     �     �  *   �  9     ;   M  &   �  6   �  >   �  -   &     !       
                	      #   %                "                                                           &                                  $   (         '                  API key API key not set for this gateway API key/Password not set for this gateway API password API username CURL extension not found in your server. please enable curl extension. Class SoapClient not found. please enable php_soap in your php. Country Credit Gateway Gateway API Gateway not set. Gateways Info Invalid API token provided in the settings. Please get your token from the <a href="https://gatewayapi.com/app" target="_blank">GatewayAPI Dashboard &rarr;</a> Mobile Number No response Outbox Please select a Gateway: %s Please set the Student mobile number field: %s Please set the User mobile number field: %s Recipients SMS SMS not sent. SMS sent to %d students. SMS sent to %d users. Send Send SMS to Selected Students Send SMS to Selected Users Send again Sender Sender name Server API Unavailable Student mobile number field Student mobile number field not set. The <code>%s</code> function is not active in your server. User mobile number field User mobile number field not set. Username/Password not set for this gateway Your account does not have credit to send SMS. Project-Id-Version: SMS module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:35+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Llave del API (key/token) Llave del API no configurada para este portal Llave del API/Contraseña no configurados para este portal Contraseña del API Nombre de usuario del API Extensión CURL no encontrada en su servidor. Por favor active la extensión curl en PHP. Calse Soapclient no encontrada. Por favor active php_soap en PHP. Pais Crédito Portal API del Portal Portal no configurado. Informaciones Portales Token del API en la configuración invalido. Por favor use el token del API disponible en <a href="https://gatewayapi.com/app" target="_blank">GatewayAPI Dashboard &rarr;</a> Número de Móvil Ninguna respuesta Bandeja de Salida Por favor seleccione un Portal: %s Por favor configure el Campo de número de móvil del Estudiante: %s Por favor configure el Campo de número de móvil del Usuario: %s Destinatarios SMS SMS no enviado. SMS enviado a %d estudiantes. SMS enviado a %d usuarios. Enviar Enviar SMS a los Estudiantes Seleccionados Enviar SMS a los Usuarios Seleccionados Enviar de nuevo Expedidor Nombre del expedidor API del servidor no disponible Campo del número de móvil del Estudiante Campo de número de móvil del Estudiante no configurado. La función <code>%s</code> no está activa en su servidor. Campo de número de móvil del Usuario Campo de número de móvil del Usuario no configurado. Nombre de usuario/Contraseña no configurados para este portal Su cuenta no tiene créditos para enviar SMS. 