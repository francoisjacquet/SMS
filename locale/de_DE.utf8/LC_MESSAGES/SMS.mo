��    )      d  ;   �      �     �      �  )   �     �     �  F     ?   M     �     �     �     �     �     �     �  �   �     �     �     �     �  .   �  +   �  
         +     /     =     V     l     q     �  
   �     �     �     �     �  $   �  :         [  !   t  *   �  .   �  �  �     �	  2   �	  ;   
     W
     d
  \   u
  Z   �
     -     2     ;  	   R     \     j     �  �   �     D     T     b  %   n  +   �  +   �     �     �       #     #   9     ]  ,   d  ,   �     �     �     �     �     	  %   #  >   I     �  %   �  9   �  C         (      &                                                                   #                             "      )   !                                    '         
              	             $       %    API key API key not set for this gateway API key/Password not set for this gateway API password API username CURL extension not found in your server. please enable curl extension. Class SoapClient not found. please enable php_soap in your php. Country Credit Does not set any gateway Gateway Gateway API Gateway not set. Gateways Info Invalid API token provided in the settings. Please get your token from the <a href="https://gatewayapi.com/app" target="_blank">GatewayAPI Dashboard &rarr;</a> Mobile Number No response Outbox Please select a Gateway: %s Please set the Student mobile number field: %s Please set the User mobile number field: %s Recipients SMS SMS not sent. SMS sent to %d students. SMS sent to %d users. Send Send SMS to Selected Students Send SMS to Selected Users Send again Sender Sender name Server API Unavailable Student mobile number field Student mobile number field not set. The <code>%s</code> function is not active in your server. User mobile number field User mobile number field not set. Username/Password not set for this gateway Your account does not have credit to send SMS. Project-Id-Version: SMS module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:35+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 API-Schlüssel API-Schlüssel für diesen Betreiber nicht gesetzt API-Schlüssel/Passwort für diesen Betreiber nicht gesetzt API-Passwort API-Benutzername Erweiterung CURL nicht gefunden auf Ihrem Server. Bitte aktivieren Sie die CURL-Erweiterung. Klasse SoapClient nicht gefunden. Bitte aktivieren Sie php_soap in Ihrer PHP-Installation. Land Guthaben Setzt keinen Betreiber Betreiber Betreiber-API Betreiber nicht gesetzt. Betreiberinformationen Ungültiges API-Token in den Einstellungen gesetzt. Bitte beziehen Sie Ihr Token vom <a href="https://gatewayapi.com/app" target="_blank">GatewayAPI Dashboard &rarr;</a> Mobilfunknummer Keine Antwort Postausgang Bitte wählen Sie einen Betreiber: %s Bitte Setzen das Schüler-Mobilfunkfeld: %s Bitte Setzen das Benutzer-Mobilfunkfeld: %s Empfänger*innen SMS SMS nicht gesendet. SMS versendet an %d Schüler*innen. SMS versendet an %d Benutzer*innen. Senden SMS versenden an ausgewählte Schüler*innen SMS versenden an ausgewählte Benutzer*innen Envoyer de nouveau Absenderkennung Absenderkennung Server-API unerreichbar Stundenten-Rufnummernfeld Schüler-Mobilfunkfeld nicht gesetzt. Die Funktion <code>%s</code> ist auf Ihrem Server nicht aktiv. Benutzer-Rufnummernfeld Benutzer-Mobilfunkfeld nicht gesetzt. Benutzername/Passwort für diesen Betreiber nicht gesetzt Ihr Konto weist nicht ausreichend Guthaben auf um SMS zu versenden. 