/**
 * Send program JS
 *
 * @package SMS module
 */

// Count characters: SMS limit is 160 characters.
$( "#text" ).on('keyup', function() {
	$( "#text-characters-count" ).text( $(this).val().length );
});

$( "#text-characters-count" ).text( $( "#text" ).val().length );
