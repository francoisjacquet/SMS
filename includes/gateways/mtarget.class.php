<?php

class mtarget extends SMS {
	private $wsdl_link = "https://api.mtarget.fr/";
	public $tariff = "http://mtarget.fr/";
	public $unitrial = true;
	public $unit;
	public $flash = "enable";
	public $isflash = false;

	public function __construct() {
		parent::__construct();
		$this->validateNumber = "Number of the recipient with country code (+44...) or in international format (0044 ...)";
	}

	public function SendSMS() {
		// Check gateway credit
		if ( ! $this->GetCredit() ) {
			return new SMS_Error( 'account-credit', dgettext( 'SMS', 'Your account does not have credit to send SMS.' ) );
		}

		$msg = urlencode( $this->msg );

		/**
		 * Modify sender number
		 *
		 * @since 3.4
		 *
		 * @param string $this ->from sender number.
		 */


		/**
		 * Modify Receiver number
		 *
		 * @since 3.4
		 *
		 * @param array $this ->to receiver number
		 */


		/**
		 * Modify text message
		 *
		 * @since 3.4
		 *
		 * @param string $this ->msg text message.
		 */

		// @link https://developers.m-target.com/#/operations/sendASimpleSMSMessage
		$msisdns = [];

		foreach ( (array) $this->to as $to )
		{
			$msisdns[] = [ 'msisdn' => $to ];
		}

		$args = array(
			'headers' => array(
				'X-API-KEY' => $this->has_key,
				'Accept'  => 'application/json',
				'Content-Type'  => 'application/json',
			),
			'body' => json_encode( array(
				'msisdns' => json_encode( $msisdns ),
				'sender' => $this->from,
				'msg'   => $this->msg,
				'allowunicode' => 'true',
			) )
		);

		$response = wp_remote_post( $this->wsdl_link . "messages", $args );

		// Ger response code
		$response_code = wp_remote_retrieve_response_code( $response );

		// check response have error or not
		if ( empty( $response['body'] ) ) {
			return false;
		}

		// Decode response
		$result = json_decode( $response['body'] );

		if ( ! empty( $result->results[0]->reason )
			&& $result->results[0]->reason == 'ACCEPTED' ) {
			return true;
		}

		return new SMS_Error( 'send-sms', $result->results[0]->reason );
	}

	public function GetCredit() {
		// Check username and password
		if ( ! $this->has_key ) {
			return new SMS_Error( 'account-credit', dgettext( 'SMS', 'API key not set for this gateway' ) );
		}

		$args = array(
			'headers' => array(
				'X-API-KEY' => $this->has_key,
				'Accept'  => 'application/json',
			),
		);

		$response = wp_remote_get( $this->wsdl_link . "balance", $args );

		// check response have error or not
		if ( empty( $response['body'] ) ) {
			return false;
		}

		// Decode response
		$result = json_decode( $response['body'] );

		return $result->amount;
	}
}
