<?php

class labsmobile extends SMS {
	private $wsdl_link = "https://api.labsmobile.com/";
	public $tariff = "http://www.labsmobile.com/";
	public $unitrial = false;
	public $unit;
	public $flash = "disable";
	public $isflash = false;

	public function __construct() {
		parent::__construct();
		$this->help           = 'Username: Email user name that identifies the sending account. Password: API token generated in the API Configuration section of the account.';
		$this->validateNumber = "34XXXXXXXXX";
		$this->has_key        = true;
	}

	public function SendSMS() {
		// Check gateway credit
		if ( ! $this->GetCredit() ) {
			return new SMS_Error( 'account-credit', dgettext( 'SMS', 'Your account does not have credit to send SMS.' ) );
		}

		/**
		 * Modify sender number
		 *
		 * @since 3.4
		 *
		 * @param string $this ->from sender number.
		 */


		/**
		 * Modify Receiver number
		 *
		 * @since 3.4
		 *
		 * @param array $this ->to receiver number
		 */


		/**
		 * Modify text message
		 *
		 * @since 3.4
		 *
		 * @param string $this ->msg text message.
		 */

		// @link https://www.labsmobile.com/en/sms-api/api-versions/http-rest-get
		$response = wp_remote_get( $this->wsdl_link . "get/send?username=" . urlencode( $this->username ) . "&password=" . $this->password . "&sender=" . urlencode( $this->from ) . "&message=" . urlencode( $this->msg ) . "&msisdn=" . implode( ',', $this->to ) );

		// Ger response code
		$response_code = wp_remote_retrieve_response_code( $response );

		$result = $response['body'];

		if ( $this->_xml_extract( "code", $result ) == "0" ) {
			$this->InsertToDB( $this->from, $this->msg, $this->to );

			/**
			 * Run hook after send sms.
			 *
			 * @since 2.4
			 *
			 * @param string $result result output.
			 */


			return $result;
		} else {
			return new SMS_Error( 'send-sms', $this->_xml_extract( 'message', $result ) );
		}
	}

	public function GetCredit() {
		// Check username and password
		if ( ! $this->username || ! $this->password ) {
			return new SMS_Error( 'account-credit', dgettext( 'SMS', 'Username/Password not set for this gateway' ) );
		}

		// @link https://www.labsmobile.com/en/sms-api/api-versions/http-rest-get
		$response = wp_remote_get( $this->wsdl_link . "get/balance?username=" . urlencode( $this->username ) . "&password=" . $this->password );

		// Ger response code
		$response_code = wp_remote_retrieve_response_code( $response );

		$result = $response['body'];

		if ( $response_code != '200' ) {
			return new SMS_Error( 'account-credit', $this->_xml_extract( 'title', $result ) );
		}

		return $this->_xml_extract( "messages", $result );
	}

	private function _xml_extract( $attr, $xml ) {
		$init     = stripos( $xml, "<" . $attr . ">" );
		$end_pos  = stripos( $xml, "</" . $attr . ">" );
		$init_pos = $init + strlen( $attr ) + 2;

		return substr( $xml, $init_pos, $end_pos - $init_pos );
	}
}
